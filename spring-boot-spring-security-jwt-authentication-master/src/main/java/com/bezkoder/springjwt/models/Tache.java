package com.bezkoder.springjwt.models;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "taches")
public class Tache implements Serializable {

	// declaration des attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ta")
	private Long idTache;

	// @Temporal(TemporalType.ZoneDateTime)
	private ZonedDateTime start;
	private String title;
	private String description;
	private boolean statutAudience;
	
	private ZonedDateTime end;

	@ManyToMany
	@JoinTable(name = "tache_client", joinColumns = @JoinColumn(name = "ta_id"), inverseJoinColumns = @JoinColumn(name = "id"))
	private List<User> utilisateurs;

	@ManyToOne
	@JoinColumn(name = "a_id", referencedColumnName = "id_a")
	private Affaire affaire;

	@ManyToOne
	@JoinColumn(name = "t_id", referencedColumnName = "id_t")
	private Tribunal tribunal;

	@OneToMany(mappedBy = "tache")
	private List<Phase> phases;

	public Tache() {
		super();
	}

	public Tache(Long idTache, ZonedDateTime start, String title, String description, boolean statutAudience,
			ZonedDateTime end) {
		super();
		this.idTache = idTache;
		this.start = start;
		this.title = title;
		this.description = description;
		this.statutAudience = statutAudience;
		this.end = end;
	}

	public Tache(ZonedDateTime start, String title, String description, boolean statutAudience, ZonedDateTime end) {
		super();
		this.start = start;
		this.title = title;
		this.description = description;
		this.statutAudience = statutAudience;
		this.end = end;
	}

	public Long getIdTache() {
		return idTache;
	}

	public void setIdTache(Long idTache) {
		this.idTache = idTache;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public void setStart(ZonedDateTime start) {
		this.start = start;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isStatutAudience() {
		return statutAudience;
	}

	public void setStatutAudience(boolean statutAudience) {
		this.statutAudience = statutAudience;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(ZonedDateTime end) {
		this.end = end;
	}

	public List<User> getUtilisateurs() {
		return utilisateurs;
	}

	public void setUtilisateurs(List<User> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	public Affaire getAffaire() {
		return affaire;
	}

	public void setAffaire(Affaire affaire) {
		this.affaire = affaire;
	}

	public Tribunal getTribunal() {
		return tribunal;
	}

	public void setTribunal(Tribunal tribunal) {
		this.tribunal = tribunal;
	}

	public List<Phase> getPhases() {
		return phases;
	}

	public void setPhases(List<Phase> phases) {
		this.phases = phases;
	}

	@Override
	public String toString() {
		return "Tache [idTache=" + idTache + ", start=" + start + ", title=" + title + ", description=" + description
				+ ", statutAudience=" + statutAudience + ", end=" + end + "]";
	}
	
	


}
