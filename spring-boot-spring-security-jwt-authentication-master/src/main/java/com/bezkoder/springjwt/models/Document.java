package com.bezkoder.springjwt.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table (name = "documents")
public class Document implements Serializable {
	
	// déclarations des attributs
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id_d")
	private Long idDocument;
	private Date dateCreation;
	private String nom;
	private String description;
	
	@ManyToOne
	@JoinColumn (name = "a_idd",referencedColumnName ="id_a")
	private Affaire affaireD;
	
	// constructeurs
	public Document() {
		super();
	}
	public Document(Long idDocument, Date dateCreation, String nom, String description) {
		super();
		this.idDocument = idDocument;
		this.dateCreation = dateCreation;
		this.nom = nom;
		this.description = description;
	}
	public Document(Date dateCreation, String nom, String description) {
		super();
		this.dateCreation = dateCreation;
		this.nom = nom;
		this.description = description;
	}
	
	// getters et setters 
	public Long getIdDocument() {
		return idDocument;
	}
	public void setIdDocument(Long idDocument) {
		this.idDocument = idDocument;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Affaire getAffaireD() {
		return affaireD;
	}
	public void setAffaireD(Affaire affaireD) {
		this.affaireD = affaireD;
	}
	
	
	// redef
	@Override
	public String toString() {
		return "Document [idDocument=" + idDocument + ", dateCreation=" + dateCreation + ", nom=" + nom
				+ ", description=" + description + "]";
	}
	
	

}
