package com.bezkoder.springjwt.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.springjwt.models.Affaire;
import com.bezkoder.springjwt.models.Tache;
import com.bezkoder.springjwt.zservice.IAffaireService;
import com.bezkoder.springjwt.zservice.IServiceTache;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test/")
public class TestController {

	@Autowired //
	private IServiceTache tService;//
	@Autowired
	private IAffaireService affaireService;

	@GetMapping("all")
	public String allAccess() {
		return "Public Content.";
	}

	@GetMapping("avocat")
	@PreAuthorize("hasRole('ROLE_AVOCAT')")
	public String avocatAccess() {
		return "avocat Content.";
	}

	@GetMapping("juridique")
	@PreAuthorize("hasRole('ROLE_JURIDIQUE')")
	public String juridiqueAccess() {
		return "juridique Board.";
	}

	// @PreAuthorize("hasRole('ROLE_JURIDIQUE')") // ajouter les avocats
	@GetMapping(path = "getTaches", produces = "application/json")
	public List<Tache> getTaches() {
		List<Tache> taches = tService.getTaches();
		System.out.println(taches);
		return taches;
	}

	@PostMapping(path = "addTache", produces = "application/json")
	public Tache addTache(@RequestBody Tache tache) {
		Tache tIn = tService.addTache(tache);
		return tIn;
	}

	@PutMapping(path = "updateTache", produces = "application/json", consumes = "application/json")
	public Tache updateTaches(@RequestBody Tache tache) {
		Tache tIn = tService.updateTache(tache);

		System.out.println(tache);
		System.out.println(tIn);
		return tIn;
	}

	@DeleteMapping(path = "deleteTache/{pId}")
	public void deleteTache(@PathVariable("pId") Long id) {
		System.out.println(id);
		tService.deleteTache(id);

	}

	@GetMapping(value = "getAllAffaire", produces = "application/json")
	public List<Affaire> getAllAffaire() {

		List<Affaire> affaires = affaireService.getAllAffaire();

		return affaires;
	}

	
	@PostMapping(value = "addAffaire")
	public Affaire addAffaire( @RequestBody Affaire affaire) {

		Affaire affaireOut = affaireService.addAffaire(affaire);

		return affaireOut;
	}

}
