package com.bezkoder.springjwt.zRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bezkoder.springjwt.models.Tache;


@Repository
public interface ITacheDao extends JpaRepository<Tache,Long> {

}
