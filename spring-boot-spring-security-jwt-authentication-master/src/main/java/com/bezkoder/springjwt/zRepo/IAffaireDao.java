package com.bezkoder.springjwt.zRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bezkoder.springjwt.models.Affaire;

@Repository
public interface IAffaireDao extends JpaRepository<Affaire,Long>{

}
