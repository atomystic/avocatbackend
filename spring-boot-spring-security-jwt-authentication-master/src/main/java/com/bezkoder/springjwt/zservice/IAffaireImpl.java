package com.bezkoder.springjwt.zservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bezkoder.springjwt.models.Affaire;
import com.bezkoder.springjwt.zRepo.IAffaireDao;

@Service
public class IAffaireImpl implements IAffaireService {

	@Autowired
	private IAffaireDao affaireDao;

	@Override
	public List<Affaire> getAllAffaire() {

		List<Affaire> affaires = affaireDao.findAll();

		return affaires;
	}

	@Override
	public Affaire addAffaire(Affaire affaire) {
		
		
	Affaire affaireOut  = affaireDao.save(affaire);
		return affaireOut;
	}

}
