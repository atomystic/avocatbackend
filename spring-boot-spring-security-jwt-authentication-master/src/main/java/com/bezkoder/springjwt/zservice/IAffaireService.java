package com.bezkoder.springjwt.zservice;

import java.util.List;

import com.bezkoder.springjwt.models.Affaire;

public interface IAffaireService {
	
public List<Affaire>  getAllAffaire();

public Affaire addAffaire (Affaire affaire);
}
