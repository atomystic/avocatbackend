package com.bezkoder.springjwt.zservice;

import java.util.List;

import com.bezkoder.springjwt.models.Tache;


public interface IServiceTache {
	
public List<Tache> getTaches();

public Tache  addTache(Tache tache);

public Tache updateTache(Tache tache);

public void deleteTache(Long id);



}
