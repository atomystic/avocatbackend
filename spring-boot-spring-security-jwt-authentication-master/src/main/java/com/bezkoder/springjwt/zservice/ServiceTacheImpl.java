package com.bezkoder.springjwt.zservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bezkoder.springjwt.models.Tache;
import com.bezkoder.springjwt.zRepo.ITacheDao;

@Service
public class ServiceTacheImpl implements IServiceTache {

	@Autowired
	private ITacheDao tDao;

	@Override
	public List<Tache> getTaches() {

		List<Tache> taches = tDao.findAll();

		return taches;
	}

	@Override
	public Tache addTache(Tache tache) {

		Tache tIn = tDao.save(tache);
		return tIn;
	}

	@Override
	public Tache updateTache(Tache tache) {

		Tache tIn = tDao.save(tache);
		return tIn;
	}

	@Override
	public void deleteTache(Long id) {
		tDao.deleteById(id);

	}

}
